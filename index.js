const express = require('express');
const config = require('./config');
const redis = require('redis').createClient(config.redisOptions);
const cors = require('cors');

const app = express();

app.use(cors());

//

app.get('/cases', (req, res) => {
  redis.lrange('cases', 0, -1, (error, list) => {
    if (error) {
      res.status(500).json(error);
    }

    res.json(list.map(caseItem => JSON.parse(caseItem)));
  });
});

const port = process.env.PORT || 80;
app.listen(port);

module.exports = express;
